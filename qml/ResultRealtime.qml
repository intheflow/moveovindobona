import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0

import "UIConstants.js" as UIConstants
import "ExtrasConstants.js" as ExtrasConstants

Item {
    id: resultRealtime
    
    property bool canRefresh: false;
    
    function clear() {
        clearTimer.start()
    }
    
    function refresh() {
        if(canRefresh) {
            canRefresh = false;
            resultModel.refresh()
        }
    }
    
    Timer {
        id: clearTimer
        interval: 300;
        onTriggered: {
            canRefresh = false;
            resultModel.clear()
        }
    }

    Component {
        id: departureDelegate

        Item {
            width: parent.width
            height: 80

            Item {
                anchors.fill: parent
                anchors.margins: UIConstants.DEFAULT_MARGIN

                Row {
                    spacing: 10
                    Text {
                        id: l
                        text: model.line // <----
                        anchors.verticalCenter: parent.verticalCenter
                        //width: 70
                        font.pixelSize: UIConstants.FONT_XLARGE
                        font.bold: true
                        font.family: ExtrasConstants.FONT_FAMILY_LIGHT
                        color: !theme.inverted ? UIConstants.COLOR_FOREGROUND : UIConstants.COLOR_INVERTED_FOREGROUND
                    }

                    Column {
                        anchors.verticalCenter: parent.verticalCenter

                        Text {
                            id: s
                            text: model.station // <----
                            width: parent.parent.parent.width - l.width - dep.width - 15
                            elide: Text.ElideRight
                            font.pixelSize: UIConstants.FONT_LARGE
                            font.family: ExtrasConstants.FONT_FAMILY_LIGHT
                            color: !theme.inverted ? UIConstants.COLOR_FOREGROUND : UIConstants.COLOR_INVERTED_FOREGROUND
                        }

                        Text {
                            id: d
                            text: model.direction // <---   -
                            width: parent.parent.parent.width - l.width - dep.width - 15
                            elide: Text.ElideRight
                            color: !theme.inverted ? UIConstants.COLOR_SECONDARY_FOREGROUND : UIConstants.COLOR_INVERTED_SECONDARY_FOREGROUND
                            font.family: ExtrasConstants.FONT_FAMILY_LIGHT
                            font.pixelSize: UIConstants.FONT_LSMALL
                        }
                    }
                }
            }

            Column {
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                Text {
                    id: dep
                    text: model.time
                    anchors.right: parent.right
                    anchors.rightMargin: UIConstants.DEFAULT_MARGIN
                    font.italic: model.lowfloor
                    font.bold: true
                    font.pixelSize: UIConstants.FONT_XLARGE
                    font.family: ExtrasConstants.FONT_FAMILY_LIGHT
                    color: !theme.inverted ? UIConstants.COLOR_FOREGROUND : UIConstants.COLOR_INVERTED_FOREGROUND
                }
            }
        }
    }

    ListView {
        id: list

        width: parent.width; height: parent.height
        snapMode: ListView.SnapToItem
        model: resultModel
        delegate: departureDelegate
    }
    
    Connections {
        target: resultModel

        onRefresh_done: {
            canRefresh = true
        }
    }
    
    BusyIndicator {
        id: busyIndicator
        visible: !canRefresh
        running: visible
        platformStyle: BusyIndicatorStyle { size: 'large' }
        anchors.centerIn: parent
    }

    ScrollDecorator {
        id: scrolldecorator
        flickableItem: list
        platformStyle: ScrollDecoratorStyle {}
    }
}
