import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0
import QtMobility.location 1.1

PageStackWindow {
    id: appWindow

    initialPage: mainPage

    MainPage {
        id: mainPage
    }
    
    ResultPage {
        id: resultPage
    }
    
    InfoBanner {
        id: errorBanner
        topMargin: 50
        timerShowTime: 2000
        
        signal errorNachricht(string message)
        
        Component.onCompleted: messageProxy.error.connect(errorNachricht)
        
        onErrorNachricht: {
            errorBanner.text = message
            errorBanner.show()
        }
    }
    
    PositionSource {
        id: positionSource
        updateInterval: 10000

        active: true

        onPositionChanged: {
            nearbyModel.update_nearby(positionSource.position.coordinate.latitude, positionSource.position.coordinate.longitude)
        }
    }
    
    SelectionDialog {
        id: nearbySelector
        titleText: 'Select nearby station'

        model: nearbyModel

        onAccepted: {
            resultModel.set_station(model.getString(nearbySelector.selectedIndex,'name'))
            pageStack.push(resultPage)
            nearbySelector.close()
        }
    }
    
    ToolBarLayout {
        id: noTools
        
        ToolIcon {
            platformIconId: 'icon-m-toolbar-search'
            anchors.left: parent.left
            onClicked: nearbySelector.open()
        }
    }

    ToolBarLayout {
        id: commonTools
        visible: false

        ToolIcon { 
            iconId: "toolbar-back";
            onClicked: {
                pageStack.pop();
                resultPage.clear()
            }
        } 

        ToolIcon {
            platformIconId: 'icon-m-toolbar-refresh'
            anchors.centerIn: parent
            onClicked: resultPage.refresh()
        }
    }
}
