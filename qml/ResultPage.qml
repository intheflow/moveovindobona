import QtQuick 1.1
import com.nokia.meego 1.0

import "UIConstants.js" as UIConstants
import "ExtrasConstants.js" as ExtrasConstants

Page {
    tools: commonTools

    function clear() {
        realtimeResult.clear()
    }

    function refresh() {
        realtimeResult.refresh()
    }

    Rectangle {
        id: header
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: -1
        }
        border {
            color: 'black'
            width: 1
        }

        gradient: Gradient {
            GradientStop { position: 0; color: '#777' }
            GradientStop { position: 1; color: '#aaa' }
        }

        height: 80
        color: 'white'
        
    }

    ResultRealtime {
        id: realtimeResult

        anchors {
            margins: 10
            top: header.bottom
            left: parent.left
            bottom: parent.bottom
            right: parent.right
        }
    }
}
