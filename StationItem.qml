// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Component {
    Rectangle {
        height: 100
        color: "lightsteelblue"

        anchors {
            left: parent.left
            right: parent.right
        }

        Text {
            id: station_name
            font.pixelSize: 20
            anchors.centerIn: parent
            text: model.name
        }

        MouseArea {
            id: mouseStation
            anchors.fill: parent
            onClicked: {
                resultModel.set_station(station_name.text)
                resultModel.clear()
                flipable.flipped = true
            }

            onPressed: {
                parent.color = "lightblue"
            }

            onReleased: {
                parent.color = "lightsteelblue"
            }
        }
    }
}
