// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import QtDesktop 0.1

Item {
    anchors.fill: parent
    ListView {
        snapMode: ListView.SnapToItem
        transformOrigin: Item.Center
        flickableDirection: Flickable.AutoFlickDirection

        anchors {
            top: parent.top
            bottom: btnRefresh.top
            left: parent.left
            right: parent.right
        }

        model: DepartModel {}
        delegate: DepartItem {}
    }

    Button {
        id: btnBack
        anchors {
            bottom: parent.bottom
            left: parent.left
        }
        text: 'Back'
        onClicked: flipable.flipped = false
    }

    Button {
        id: btnRefresh
        anchors {
            bottom: parent.bottom
            left: btnBack.right
        }
        text: 'Refresh'
        onClicked: resultModel.refresh()
    }
}
