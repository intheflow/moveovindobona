// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import QtDesktop 0.1

Item {
    anchors.fill: parent
    ListView {
        snapMode: ListView.SnapToItem
        transformOrigin: Item.Center
        flickableDirection: Flickable.AutoFlickDirection

        anchors {
            top: txtFilter.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        model: stationModel
        delegate: StationItem {}
    }

    Label {
        id: lblFilter
        text: "Filter: "
        font.pixelSize: 20
        anchors {
            left: parent.left
            top: parent.top
        }
    }

    TextField {
        id: txtFilter
        focus: true
        anchors {
            left: lblFilter.right
            right: parent.right
            top: parent.top
        }
        Keys.onReturnPressed: {
            stationModel.filter_stations(text)
        }
    }
}
