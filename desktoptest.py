from moveoVindobona import realtime
from qmlhacks import QmlDataModel

import settings

import sys, os, csv, threading
from PySide.QtCore import QObject, Slot, Signal
from PySide.QtGui import QApplication
from PySide.QtDeclarative import QDeclarativeView

class DepartureModel(QmlDataModel):
    def __init__(self, station_dict, parent=None):
        keys = ('line','station','direction','time', 'lowfloor')
        super(DepartureModel, self).__init__(keys, parent)
        self._station_dict = station_dict

    @Slot()
    def clear(self):
        self.setData(())

    @Slot()
    def refresh(self):
        def refresh_async():
            self._dep_list.refresh()
            self.refresh_data()
            self.refresh_done.emit()
            
        threading.Thread(target=refresh_async).start()
        
    def refresh_data(self):
        self.setData(map(lambda x: (x["Linie"], self._station, x["Endstation"], 
            int((x["AbfahrtPrognose"] - self._dep_list.timestamp).seconds / 60),
            x["lowfloor"]), self._dep_list))
    
    @Slot(unicode)
    def set_station(self, station):
        def set_station_async(station):
            self._station = station
            self._dep_list = realtime.DepartureList(settings.sender, self._station_dict[station])
            self.refresh_data()
            self.refresh_done.emit()
        
        threading.Thread(target=set_station_async, args=(station,)).start()

    refresh_done = Signal()

resultModel = None

class StationModel(QmlDataModel):
    def __init__(self, parent=None):
        keys = ('name', )
        self.baseData = []
        super(StationModel, self).__init__(keys, parent)

    @Slot(unicode)
    def filter_stations(self, filter_string):
        self.setData(filter(lambda x: filter_string.lower() in x[0].lower(), self.baseData))

    def setBaseData(self, data):
        self.baseData = data
        self.setData(data)

if __name__ == '__main__':
    stations = {}
    stations_gps = []
    csv_dict_reader = csv.DictReader(open(os.path.join(os.path.dirname(__file__), 'stations.csv'), 'rb'), delimiter=';')
    for csv_line in csv_dict_reader:
        langbez = csv_line['langbez'].decode("utf-8")
        if langbez not in stations:
            stations_gps.append({'station': langbez, 'lat': float(csv_line['X-Koordinaten'].replace(',','.')), 'lon': float(csv_line['Y-Koordinaten'].replace(',','.'))})
            stations[langbez] = (csv_line['haltepunkt'],)
        else:
            stations[langbez] = stations[langbez] + (csv_line['haltepunkt'],)
    
    app = QApplication(sys.argv)

    view = QDeclarativeView()
    
    # expose the object to QML
    context = view.rootContext()

    stationModel = StationModel()
    stationModel.setBaseData(map(lambda x: (x,), sorted(stations.keys())))
    resultModel = DepartureModel(stations)

    context.setContextProperty('resultModel', resultModel)
    context.setContextProperty('stationModel', stationModel)

    # Assume test from source directory, use relative path
    view.setSource(os.path.join(os.path.dirname(__file__), 'oepnvScreen.qml'))

    view.setResizeMode(QDeclarativeView.SizeRootObjectToView)
    view.show()
    sys.exit(app.exec_())
