// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import QtDesktop 0.1

ApplicationWindow {
    id: appWindow

    width: 400; height: 600

    Timer {
        interval: 30000; running: true; repeat: true
        //onTriggered: {
        //    resultModel.refresh()
        //}
    }

    Flipable {
        id: flipable
        property bool flipped: false

        smooth: true

        anchors.fill: parent
        front: MainPage {}
        back: ResultPage {}

        transform: Rotation {
            id: rotation
            origin.x: flipable.width/2
            origin.y: flipable.height/2
            axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
            angle: 0    // the default angle
        }

        states: State {
            name: "back"
            PropertyChanges { target: rotation; angle: 180 }
            when: flipable.flipped
        }

        transitions: Transition {
            NumberAnimation { target: rotation; property: "angle"; duration: 500 }
        }

    }
}
